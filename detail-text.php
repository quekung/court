<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top.html") ?>
<script type="text/javascript">
//<![CDATA[
document.write('<link href="cs/animate.css" rel="stylesheet" type="text/css">');
//]]>
</script>

<!-- /Top Head -->

<body>
<script>
  //<![CDATA[
  $(document).ready(function(){
	  $('#navigation>ul>li:nth-child(2)>a').addClass('selected');
  });
  //]]>
</script>
<!-- Headbar -->
<?php include("incs/header.html") ?>
<!-- /Headbar -->
<div class="page-category">


   <div id="toc">
		<div class=" sec-detail">
		<figure class="cover"><a href="di/banner/cover-article-full.png" data-fancybox="gcover"><img src="di/banner/cover-article.png"></a></figure>
		<div class="container">
			<article class="reader">
				<header class="hgroup">
					<p class="tag"><a href="category.php">บทความ</a></p>
					<h1>จาก รสช. สู่ คสช. เมื่อแพตเทิร์นแห่งการรัฐประหารเริ่มที่ทหารทะเลาะกับนักการเมือง</h1>
					<span class="date">15/01/2563</span>
					<div class="tool-bar between-xs">
						<div class="share">
							<span>แชร์ </span>
							<div class="list">
							<a href="#" title="facebook"><i class="ic-sh-fb"></i></a>
							<a href="#" title="facebook"><i class="ic-sh-line"></i></a>
							<a href="#" title="facebook"><i class="ic-sh-tw"></i></a>
							</div>
						</div>
						<div class="view"><i class="ic-view"></i> 678</div>
					</div>
				</header>
				<div class="text-hl">
					<h2>Hightlight</h2>
					<ul>
						<li>ย้อนกลับไปช่วงปี 2533 น้าชาติ-พล.อ.ชาติชาย ชุณหะวัณ ขึ้นเป็นนายกรัฐมนตรี ได้ราว 2 ปี หลังจากยุค ‘ประชาธิปไตยครึ่งใบ’ ของ พล.อ.เปรม ติณสูลานนท์ ที่จบลงด้วยคำว่า “ผมพอแล้ว” ซึ่งถือเป็นฉากที่เซอร์ไพร์สคนไทยมาก เพราะก่อนหน้านั้น ไม่ว่าพรรคการเมืองไหนชนะการเลือกตั้ง ก็มีอันว่าต้องไปเชิญ ‘ป๋า’ มาเป็นนายกฯ</li>
						<li>11.00 น. วันที่ 23 กุมภาพันธ์ 2534 กองทัพก็จัดการ ‘รัฐประหาร’ รัฐบาล พล.อ.ชาติชาย เสร็จสรรพ ระหว่างที่ขึ้นเครื่องบินจากสนามบิน บน.6 พา พล.อ.อาทิตย์ กำลังเอก รองนายกรัฐมนตรีไปเข้าเฝ้าฯ ที่เชียงใหม่ ภายใต้ชื่อ ‘คณะรักษาความสงบเรียบร้อยแห่งชาติ’ หรือ รสช.</li>
					</ul>
				</div>
				<div class="read-body editor">
					<p class="f-normal t-black">ถ้าย้อนประวัติศาสตร์การเมืองไทย เราจะพบว่าเหตุการณ์ที่บ่มเพาะก่อนจะเดินทางไปสู่การรัฐประหารหรือการแย่งชิงอำนาจระหว่าง ‘ทหาร’ และ ‘นักการเมือง’ นั้น มีเส้นทางเฉพาะที่ไม่ได้แตกต่างกันมากนัก รูปแบบยังคงเดิมแม้เวลาจะเปลี่ยนไป ที่เปลี่ยนคือ ‘ตัวละคร’ ที่เข้ามาใหม่</p>
 
					<p>ย้อนกลับไปช่วงปี 2533 น้าชาติ-พล.อ.ชาติชาย ชุณหะวัณ ขึ้นเป็นนายกรัฐมนตรี ได้ราว 2 ปี หลังจากยุค ‘ประชาธิปไตยครึ่งใบ’ ของ พล.อ.เปรม ติณสูลานนท์ ที่จบลงด้วยคำว่า “ผมพอแล้ว” ซึ่งถือเป็นฉากที่เซอร์ไพร์สคนไทยมาก เพราะก่อนหน้านั้น ไม่ว่าพรรคการเมืองไหนชนะการเลือกตั้ง ก็มีอันว่าต้องไปเชิญ ‘ป๋า’ มาเป็นนายกฯ</p>

					<p>สุดท้าย น้าชาติเลยได้ขึ้นเป็นนายกฯ ที่มาจากพรรคการเมือง มาจากการเลือกตั้งจริงจังครั้งแรก นับตั้งแต่ปี 2519 และถือเป็นครั้งแรกที่คนไทยได้สัมผัสกับสิ่งที่เรียกว่า ‘ประชาธิปไตยเต็มใบ’ นับตั้งแต่ระบบถูกสั่นคลอนไปครั้งเหตุการณ์ 6 ตุลาฯ เป็นต้นมา</p>

					<p>ในยุคน้าชาติที่มีคำพูดติดปากว่าโนพร็อบเบล็ม นั้น ถือเป็นยุคที่เศรษฐกิจกำลัง ‘บูม’ ได้ที่ ราคาอสังหาริมทรัพย์กำลังพุ่งสูงถึงขีดสุด กลายเป็นจุดเริ่มต้นของ ‘ยุคฟองสบู่’ ซึ่งทำให้ พล.อ.ชาติชาย ได้รับคะแนนนิยมเป็นอย่างมาก</p>

 					<p>ถึงขนาด 30 ปีให้หลัง ยังคงมีคน ‘ปลุกผี’ พล.อ.ชาติชาย กลับมาหาเสียง</p>

 					<p>แต่ในทางการเมือง พล.อ.ชาติชาย ที่แม้จะมียศนำหน้าเป็นนายพลเช่นเดียวกัน ซ้ำยังมีบิดาเป็นบุคคลสำคัญระดับ ‘จอมพล’ ก็ยังต้องอาศัยบารมีของทหารในการค้ำบัลลังก์ เพราะ ‘กองทัพ’ ยังต้องการมีบทบาทนำในทางการเมือง</p>

					<p>ยุคนั้นผู้ที่มีบทบาทสำคัญในกองทัพ ได้แก่ บิ๊กสุ-พล.อ.สุจินดา คราประยูร ผู้บัญชาการทหารบก ซึ่งขึ้นมารับตำแหน่งเร็วกว่ากำหนด เพราะ ‘ลูกป๋า’ อย่าง บิ๊กจิ๋ว-พล.อ.ชวลิต ยงใจยุทธ ลาออกจากตำแหน่งก่อนเกษียณอายุเพื่อมาเล่นการเมือง โดยบิ๊กสุในสมัยนั้น ได้รับยกย่องเป็นนายทหารหัวก้าวหน้า จบการศึกษาจากสหรัฐอเมริกา พูดจาไพเราะ รูปหล่อ คารมดี</p>

					<p>ส่วนอีกคนหนึ่งที่มีบทบาทไม่แพ้กันคือ นายพลเสื้อคับ บิ๊กจ๊อด-พล.อ.สุนทร คงสมพงษ์ ผู้บัญชาการทหารสูงสุด นายทหารอดีตนักบินเฮลิคอปเตอร์แห่งกองทัพบก ผู้ยึดวลี “ไม่ฆ่าน้อง ไม่ฟ้องนาย ไม่ขายเพื่อน”</p>

 					<p>ทุกเช้าวันพุธ ที่บ้านซอยราชครูของนายกรัฐมนตรีจะมีวงกินอาหารเช้าร่วมกัน ระหว่าง พล.อ.ชาติชาย และบรรดา ‘บิ๊กๆ’ ของทั้งกองทัพ-กระทรวงกลาโหม แต่อยู่ดีๆ กลางปี 2533 ก็เกิด ‘รอยร้าว’ ขนาดใหญ่ ระหว่างรัฐบาลน้าชาติ และบรรดาผู้นำเหล่าทัพ</p>

					<p>เรื่องมันเริ่มต้นขึ้น เมื่อ ร.ต.อ.เฉลิม อยู่บำรุง รมต.ประจำสำนักนายกรัฐมนตรี หัวหน้าพรรคมวลชน ซึ่งมี ส.ส. เพียง 5 คน เริ่มบริภาษ ‘บิ๊กจิ๋ว’ ซึ่งในขณะนั้นมารับตำแหน่งรองนายกรัฐมนตรี และ รมว.กลาโหม พาดพิงว่า คุณหญิงพันเครือ ยงใจยุทธ ภริยาของ พล.อ.ชวลิต ในขณะนั้นเป็น ‘ตู้เพชรเคลื่อนที่’ ซ้ำยังวิจารณ์บทบาทของบิ๊กจิ๋วซึ่งเป็นรัฐมนตรีร่วม ครม. อยู่บ่อยครั้ง และในตอนนั้น พล.อ.ชาติชาย เองก็ไม่ได้ไม่ตัดสินใจปลดเฉลิมออกจากตำแหน่ง แต่ ‘บิ๊กจิ๋ว’ กลับลาออกไปตั้งพรรคความหวังใหม่เอง ทำให้เวลานั้นผู้นำเหล่าทัพเริ่มจะไม่พอใจบทบาทของ พล.อ.ชาติชาย ที่ปล่อยให้รัฐมนตรี ‘ซ่า’​ ไปวิจารณ์นายเก่าอย่าง พล.อ.ชวลิต และออกมาให้สัมภาษณ์ไม่พอใจรัฐบาลอยู่หลายครั้ง</p>

					<p>ขณะเดียวกัน เฉลิม ยังมี​ ‘ก๊อกสอง’ เมื่ออยู่ดีๆ กองทัพไปยึดรถ​ถ่ายทอดสดของช่อง 9 อสมท. ที่จอดอยู่บริเวณหนองแขม ใกล้เคียงกับหน่วยทหาร จนกลายเป็นข่าวดังว่าเฉลิมและรัฐบาลถูกสั่งให้นำรถโมบายล์ไปจอดเพื่อ ‘ดักฟัง’ ทหาร</p>
					
					

				</div>
			</article>

			<section class="sec-related wow fadeIn" data-wow-delay="0.5s">
				<div class="head-title border0 start-xs">
					<h2 class="h-line"><a href="#all">เรื่องอื่น ๆ ที่เกี่ยวข้อง</a></h2>
				</div>

				<div class="thm-news-list row _chd-cl-xs-12-sm-03">
					<? for($i=1;$i<=1;$i++){ ?>
					<article>
						<div class="in">
							<figure>
								<a href="detail.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."><img src="di/banner/thm-news-01.png" alt="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."></a>
							</figure>
							<div class="detail">
								<div class="cat"><a href="#" title="ข่าวประชาสัมพันธ์">ข่าวประชาสัมพันธ์</a></div>
								<h3><a href="detail.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...">ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...</a></h3>

								<div class="tools">
									<span class="date">22/01/2563</span>  | 
									<span class="view"><i class="fas fa-eye"></i> 30</span>
								</div>
							</div>
						</div>
					</article>

					<article>
						<div class="in">
							<figure>
								<a href="detail-text.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."><img src="di/banner/thm-news-02.png" alt="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."></a>
							</figure>
							<div class="detail">
								<div class="cat"><a href="#" title="ข่าวประชาสัมพันธ์">ข่าวประชาสัมพันธ์</a></div>
								<h3><a href="detail.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...">ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...</a></h3>

								<div class="tools">
									<span class="date">22/01/2563</span>  | 
									<span class="view"><i class="fas fa-eye"></i> 30</span>
								</div>
							</div>
						</div>
					</article>

					<article>
						<div class="in">
							<figure>
								<a href="detail.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."><img src="di/banner/thm-news-03.png" alt="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."></a>
							</figure>
							<div class="detail">
								<div class="cat"><a href="#" title="ข่าวประชาสัมพันธ์">ข่าวประชาสัมพันธ์</a></div>
								<h3><a href="detail.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...">ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...</a></h3>

								<div class="tools">
									<span class="date">22/01/2563</span>  | 
									<span class="view"><i class="fas fa-eye"></i> 30</span>
								</div>
							</div>
						</div>
					</article>

					<article>
						<div class="in">
							<figure>
								<a href="detail.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."><img src="di/banner/cover-news.png" alt="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."></a>
							</figure>
							<div class="detail">
								<div class="cat"><a href="#" title="ข่าวประชาสัมพันธ์">ข่าวประชาสัมพันธ์</a></div>
								<h3><a href="detail.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...">ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...</a></h3>

								<div class="tools">
									<span class="date">22/01/2563</span>  | 
									<span class="view"><i class="fas fa-eye"></i> 30</span>
								</div>
							</div>
						</div>
					</article>
					<? } ?>
				</div>


			</section>
		</div>
		
		
		
		

		</div>
  </div>
</div>
<!-- footer -->
<?php include("incs/footer.html") ?>
<?php /*?><?php include("incs/lightbox.html") ?><?php */?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js.html") ?>
<!-- /js -->

</body>
</html>
