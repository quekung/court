<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top.html") ?>
<script type="text/javascript">
//<![CDATA[
document.write('<link href="cs/animate.css" rel="stylesheet" type="text/css">');
//]]>
</script>

<!-- /Top Head -->

<body>
<script>
  //<![CDATA[
  $(document).ready(function(){
	  $('#navigation>ul>li:nth-child(2)>a').addClass('selected');
  });
  //]]>
</script>
<!-- Headbar -->
<?php include("incs/header.html") ?>
<!-- /Headbar -->
<div class="page-category">


   <div id="toc">
		<div class="container pt20-sm pt10-xs">

		
		<section class="sec-01 wow fadeIn" data-wow-delay="0.5s">
			<div class="head-title border0 start-xs">
				<h2 class="h-line"><span>บทความ</span></h2>
			</div>

			<div class="thm-news-list row _chd-cl-xs-12-sm-03">
				<? for($i=1;$i<=4;$i++){ ?>
				<article>
					<div class="in">
						<figure>
							<a href="detail.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."><img src="di/banner/thm-news-01.png" alt="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."></a>
						</figure>
						<div class="detail">
							<div class="cat"><a href="#" title="ข่าวประชาสัมพันธ์">ข่าวประชาสัมพันธ์</a></div>
							<h3><a href="detail.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...">ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...</a></h3>
							
							<div class="tools">
								<span class="date">22/01/2563</span>  | 
								<span class="view"><i class="fas fa-eye"></i> 30</span>
							</div>
						</div>
					</div>
				</article>
				
				<article>
					<div class="in">
						<figure>
							<a href="detail-text.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."><img src="di/banner/thm-news-02.png" alt="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."></a>
						</figure>
						<div class="detail">
							<div class="cat"><a href="#" title="ข่าวประชาสัมพันธ์">ข่าวประชาสัมพันธ์</a></div>
							<h3><a href="detail.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...">ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...</a></h3>
							
							<div class="tools">
								<span class="date">22/01/2563</span>  | 
								<span class="view"><i class="fas fa-eye"></i> 30</span>
							</div>
						</div>
					</div>
				</article>
				
				<article>
					<div class="in">
						<figure>
							<a href="detail.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."><img src="di/banner/thm-news-03.png" alt="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."></a>
						</figure>
						<div class="detail">
							<div class="cat"><a href="#" title="ข่าวประชาสัมพันธ์">ข่าวประชาสัมพันธ์</a></div>
							<h3><a href="detail.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...">ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...</a></h3>
							
							<div class="tools">
								<span class="date">22/01/2563</span>  | 
								<span class="view"><i class="fas fa-eye"></i> 30</span>
							</div>
						</div>
					</div>
				</article>
				
				<article>
					<div class="in">
						<figure>
							<a href="detail.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."><img src="di/banner/cover-news.png" alt="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."></a>
						</figure>
						<div class="detail">
							<div class="cat"><a href="#" title="ข่าวประชาสัมพันธ์">ข่าวประชาสัมพันธ์</a></div>
							<h3><a href="detail.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...">ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...</a></h3>
							
							<div class="tools">
								<span class="date">22/01/2563</span>  | 
								<span class="view"><i class="fas fa-eye"></i> 30</span>
							</div>
						</div>
					</div>
				</article>
				<? } ?>
			</div>
			
			<div class="_flex center-xs mt20-xs">
			<div class="pagination _self-mt0">
                            <ul class="_flex middle-xs center-xs flex-nowrap">
                              <li>
                                <a title="Previous" href="#" class="btn"><i class="fas fa-angle-left"></i> กลับ</a>
                              </li>
                              <li>
                                <a title="Previous" href="#" class="active">1</a>
                              </li>
                              <li>
                                <a title="2" href="#">2</a>
                              </li>
                              <li>
                                <a title="3" href="#">3</a>
                              </li>
                              <li>
                                <a title="4" href="#">4</a>
                              </li>
							  <li>...</li>
							  <li>
                                <a title="20" href="#">20</a>
                              </li>
                              <li>
                                <a title="Next" href="#" class="btn">ถัดไป <i class="fas fa-angle-right"></i></a>
                              </li>
							  <?php /*?><li>
								  <span class="cv-select">
									<select class="select-box bg-wh" id="type-page">
										<option value="0" selected="">10 / page</option>
										<option value="1">20 / page</option>
										<option value="2">30 / page</option>
									</select>
								</span>
							  </li><?php */?>
                            </ul>
                        </div>
		</div>
		</section>
		
		
		
		
		

		</div>
  </div>
</div>
<!-- footer -->
<?php include("incs/footer.html") ?>
<?php /*?><?php include("incs/lightbox.html") ?><?php */?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js.html") ?>
<!-- /js -->

</body>
</html>
