<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top.html") ?>
<script type="text/javascript">
//<![CDATA[
document.write('<link href="cs/animate.css" rel="stylesheet" type="text/css">');
//]]>
</script>

<!-- /Top Head -->

<body>
<script>
  //<![CDATA[
  $(document).ready(function(){
	  $('#navigation>ul>li:nth-child(2)>a').addClass('selected');
  });
  //]]>
</script>
<!-- Headbar -->
<?php include("incs/header.html") ?>
<!-- /Headbar -->
<div class="page-category">


   <div id="toc">
		<div class="sec-detail">
		<figure class="cover"><a href="di/banner/cover-news-full.png" data-fancybox="gcover"><img src="di/banner/cover-news.png"></a></figure>
		<div class="container">
			<article class="reader">
				<header class="hgroup">
					<p class="tag"><a href="category.php">ข่าวประชาสัมพันธ์</a></p>
					<h1>ประธานศาลรัฐธรรมนูญแห่งราชอาณาจักรไทยให้การต้อนรับประธานศาลรัฐธรรมนูญฮังการี และร่วมลงนามในข้อตกลงความร่วมมือทางวิชาการ (MOU)</h1>
					<span class="date">15/01/2563</span>
					<div class="tool-bar between-xs">
						<div class="share">
							<span>แชร์ </span>
							<div class="list">
							<a href="#" title="facebook"><i class="ic-sh-fb"></i></a>
							<a href="#" title="facebook"><i class="ic-sh-line"></i></a>
							<a href="#" title="facebook"><i class="ic-sh-tw"></i></a>
							</div>
						</div>
						<div class="view"><i class="ic-view"></i> 350</div>
					</div>
				</header>
				<div class="read-body editor">
					<p>เมื่อวันที่ ๑๕ มกราคม ๒๕๖๓ นายนุรักษ์ มาประณีต ประธานศาลรัฐธรรมนูญ พร้อมด้วยคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาลรัฐธรรมนูญ ร่วมให้การต้อนรับ Dr. Tamas Sulyokประธานศาลรัฐธรรมนูญฮังการี พร้อมคณะ ในโอกาสเดินทางมาเยือนศาลรัฐธรรมนูญแห่งราชอาณาจักรไทย เพื่อหารือและแลกเปลี่ยนข้อคิดเห็นทางวิชาการ และร่วมลงนามในข้อตกลงความร่วมมือทางวิชาการระหว่างศาลรัฐธรรมนูญแห่งราชอาณาจักรไทยกับศาลรัฐธรรมนูญฮังการี </p>
					<figure>
						<a href="di/banner/img-news-01.png" data-fancybox="g1"><img src="di/banner/img-news-01.png" alt="news"></a>
					</figure>
					<p>โอกาสนี้ ประธานศาลรัฐธรรมนูญฮังการี ให้เกียรติบรรยายพิเศษในหัวข้อ “The Role of the Hungarian Constitutional Court to protecting theRule of Law” โดยมีคณะตุลาการศาลรัฐธรรมนูญ ผู้บริหารสำนักงานศาลรัฐธรรมนูญ และผู้เข้ารับการอบรมหลักสูตรนิติธรรมเพื่อประชาธิปไตย (นธป.) รุ่นที่ ๘ เข้าร่วมรับฟังการบรรยายดังกล่าว ณ สำนักงานศาลรัฐธรรมนูญ ศูนย์ราชการเฉลิมพระเกียรติ ๘๐ พรรษา ๕ ธันวาคม ๒๕๕๐ อาคารราชบุรีดิเรกฤทธิ์ ถนนแจ้งวัฒนะ กรุงเทพมหานคร</p>
					
					<ul class="ft-gallery row _chd-cl-xs-06-mb20">
						<li><a href="di/banner/thm-news-01.png" data-fancybox="g1"><img src="di/banner/thm-news-01.png" alt="news"></a></li>
						<li><a href="di/banner/thm-news-02.png" data-fancybox="g1"><img src="di/banner/thm-news-02.png" alt="news"></a></li>
						<li><a href="di/banner/thm-news-03.png" data-fancybox="g1"><img src="di/banner/thm-news-03.png" alt="news"></a></li>
						<li><a href="di/banner/thm-news-04.png" data-fancybox="g1"><img src="di/banner/thm-news-04.png" alt="news"></a></li>
					</ul>
				</div>
			</article>

			<section class="sec-related wow fadeIn" data-wow-delay="0.5s">
				<div class="head-title border0 start-xs">
					<h2 class="h-line"><a href="#all">เรื่องอื่น ๆ ที่เกี่ยวข้อง</a></h2>
				</div>

				<div class="thm-news-list row _chd-cl-xs-12-sm-03">
					<? for($i=1;$i<=1;$i++){ ?>
					<article>
						<div class="in">
							<figure>
								<a href="detail.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."><img src="di/banner/thm-news-01.png" alt="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."></a>
							</figure>
							<div class="detail">
								<div class="cat"><a href="#" title="ข่าวประชาสัมพันธ์">ข่าวประชาสัมพันธ์</a></div>
								<h3><a href="detail.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...">ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...</a></h3>

								<div class="tools">
									<span class="date">22/01/2563</span>  | 
									<span class="view"><i class="fas fa-eye"></i> 30</span>
								</div>
							</div>
						</div>
					</article>

					<article>
						<div class="in">
							<figure>
								<a href="detail-text.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."><img src="di/banner/thm-news-02.png" alt="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."></a>
							</figure>
							<div class="detail">
								<div class="cat"><a href="#" title="ข่าวประชาสัมพันธ์">ข่าวประชาสัมพันธ์</a></div>
								<h3><a href="detail.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...">ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...</a></h3>

								<div class="tools">
									<span class="date">22/01/2563</span>  | 
									<span class="view"><i class="fas fa-eye"></i> 30</span>
								</div>
							</div>
						</div>
					</article>

					<article>
						<div class="in">
							<figure>
								<a href="detail.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."><img src="di/banner/thm-news-03.png" alt="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."></a>
							</figure>
							<div class="detail">
								<div class="cat"><a href="#" title="ข่าวประชาสัมพันธ์">ข่าวประชาสัมพันธ์</a></div>
								<h3><a href="detail.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...">ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...</a></h3>

								<div class="tools">
									<span class="date">22/01/2563</span>  | 
									<span class="view"><i class="fas fa-eye"></i> 30</span>
								</div>
							</div>
						</div>
					</article>

					<article>
						<div class="in">
							<figure>
								<a href="detail.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."><img src="di/banner/cover-news.png" alt="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."></a>
							</figure>
							<div class="detail">
								<div class="cat"><a href="#" title="ข่าวประชาสัมพันธ์">ข่าวประชาสัมพันธ์</a></div>
								<h3><a href="detail.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...">ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...</a></h3>

								<div class="tools">
									<span class="date">22/01/2563</span>  | 
									<span class="view"><i class="fas fa-eye"></i> 30</span>
								</div>
							</div>
						</div>
					</article>
					<? } ?>
				</div>


			</section>
		</div>
		
		
		
		

		</div>
  </div>
</div>
<!-- footer -->
<?php include("incs/footer.html") ?>
<?php /*?><?php include("incs/lightbox.html") ?><?php */?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js.html") ?>
<!-- /js -->

</body>
</html>
