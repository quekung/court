<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top.html") ?>
<script type="text/javascript">
//<![CDATA[
document.write('<link href="cs/animate.css" rel="stylesheet" type="text/css">');
//]]>
</script>

<!-- /Top Head -->

<body>
<script>
  //<![CDATA[
  $(document).ready(function(){
	  $('#navigation>ul>li:nth-child(2)>a').addClass('selected');
  });
  //]]>
</script>
<!-- Headbar -->
<?php include("incs/header.html") ?>
<!-- /Headbar -->
<div class="page-category">


   <div id="toc" class=" pt20-sm pt10-xs">
		<div class="sec-vdo container">
			<div class="podcast-view">
				<div class="view-player">
					<div class="card d-flex">
						<img src="di/banner/mini-podcast.png" alt="" class="hidden-xs">
						<div class="info _self-cl-xs">
							<p class="tag"><a href="#">พอดแคสต์</a></p>
							<h1>รัฐธรรมนูญที่ประชาชนไม่มีส่วนร่วม</h1>
						</div>
					</div>
					<div class="btn-share">
						<a class="ui-btn-gray2-sq-mini btn-xs" href="#"><i class="fas fa-share-square"></i> Share</a>
					</div>
				</div>
				<div class="audio bar">
					<div class="control d-flex flex-nowrap middle-xs">
						<div class="g-btn d-flex center-xs middle-xs">
							<i class="fas fa-2x fa-step-backward"></i>
							<i class="fas fa-3x fa-play"></i>
							<i class="fas fa-2x fa-step-forward"></i>
						</div>
						<div class="time-line _self-cl-xs">
							<div class="current" style="width: 15%"></div>
							<div class="cache" style="width: 40%"></div>
						</div>
						<div class="view-time">
						-38:07
						</div>
					</div>
					<!--<audio id="audio1" class="atn-audio" controls="" disablepictureinpicture="" controlslist="nodownload">
						<source src="https://www.w3schools.com/html/horse.ogg" type="audio/ogg">
						<source src="https://www.w3schools.com/html/horse.mp3" type="audio/mpeg">
						Your browser does not support the audio element.
					</audio>-->
					<!--<img src="di/bar-podcast.png">-->
				</div>
			</div>
			<article class="reader vdo row _chd-cl-xs-12-sm-08">
				<header class="hgroup _self-cl-xs-12-sm-04 pt0-xs">
					<p class="tag"><a href="category.php">พอดแคสต์</a></p>
					<h1>รัฐธรรมนูญที่ประชาชนไม่มีส่วนร่วม</h1>
					<span class="date">15/01/2563</span>
					<div class="tool-bar start-xs">
						<div class="share">
							<span>แชร์ </span>
							<div class="list">
							<a href="#" title="facebook"><i class="ic-sh-fb"></i></a>
							<a href="#" title="facebook"><i class="ic-sh-line"></i></a>
							<a href="#" title="facebook"><i class="ic-sh-tw"></i></a>
							</div>
						</div>
						<div class="view"><i class="ic-view"></i> 678</div>
					</div>
				</header>
				
				<div class="read-body editor">
 
					<p>คสช. ใช้เวลาสามปีกว่าหมกมุ่นอยู่กับการร่างรัฐธรรมนูญฉบับใหม่ การจัดทำร่างรัฐธรรมนูญ คสช. กำหนดกรอบการร่างไว้ในมาตรา 35 ของรัฐธรรมนูญ(ชั่วคราว) 2557 เช่น ต้องสร้างกลไกในการป้องกันและปราบปรามทุจริต ต้องมีกลไกในการป้องกันไม่ให้มีการทำลายหลักการสำคัญขอรัฐธรรมนูญ และต้องมีกลไกป้องกันไม่ให้ผู้ที่เคยถูกศาลตัดสินว่าทุจริตเข้าดำรงตำแหน่งทางการเมือง การกำหนดกรอบเช่นนี้ชัดเจนว่า คสช. มีธงในการร่างรัฐธรรมนูญของตัวเอง</p>
					<?php /*?><div class="vdo-info">
						<p><a href="#">สื่อประชาสัมพันธ์ศาลรัฐธรรมนูญ<a href="#"></p>
						<p>[<a href="#">2D animation</a> / <a href="#">Motion Graphics</a>]</p>
						<p>เรื่อง “ความสำคัญ หน้าที่ และอำนาจของศาลรัฐธรรมนูญ”</p>
						<p>ความยาว 10.57 นาที</p>
						<p>ผลิตโดย สำนักงานศาลรัฐธรรมนูญ</p>
					</div><?php */?>
				</div>
			</article>

			<section class="sec-related wow fadeIn" data-wow-delay="0.5s">
				<div class="head-title border0 start-xs">
					<h2 class="h-line"><a href="#all">เรื่องอื่น ๆ ที่เกี่ยวข้อง</a></h2>
				</div>

				<div class="thm-pod-list row _chd-cl-xs-12-sm-03">
					<? for($i=1;$i<=1;$i++){ ?>
					<article>
						<div class="in">
							<figure>
								<a href="podcast.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."><img src="di/banner/thm-podcast.png" alt="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."><i class="ic-play"></i></a>
							</figure>
							<div class="detail">
								<div class="cat"><a href="#" title="พอดแคสต์">พอดแคสต์</a></div>
								<h3><a href="detail-vdo.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...">ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...</a></h3>

								<div class="tools">
									<span class="date">22/01/2563</span>  | 
									<span class="view"><i class="fas fa-eye"></i> 30</span>
								</div>
							</div>
						</div>
					</article>

					<article>
						<div class="in">
							<figure>
								<a href="podcast.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."><img src="di/banner/thm-podcast2.png" alt="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."><i class="ic-play"></i></a>
							</figure>
							<div class="detail">
								<div class="cat"><a href="#" title="พอดแคสต์">พอดแคสต์</a></div>
								<h3><a href="detail.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...">ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...</a></h3>

								<div class="tools">
									<span class="date">22/01/2563</span>  | 
									<span class="view"><i class="fas fa-eye"></i> 30</span>
								</div>
							</div>
						</div>
					</article>

					<article>
						<div class="in">
							<figure>
								<a href="podcast.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."><img src="di/banner/thm-podcast.png" alt="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."><i class="ic-play"></i></a>
							</figure>
							<div class="detail">
								<div class="cat"><a href="#" title="พอดแคสต์">พอดแคสต์</a></div>
								<h3><a href="podcast.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...">ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...</a></h3>

								<div class="tools">
									<span class="date">22/01/2563</span>  | 
									<span class="view"><i class="fas fa-eye"></i> 30</span>
								</div>
							</div>
						</div>
					</article>

					<article>
						<div class="in">
							<figure>
								<a href="detail-vdo.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."><img src="di/banner/thm-podcast2.png" alt="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."><i class="ic-play"></i></a>
							</figure>
							<div class="detail">
								<div class="cat"><a href="#" title="พอดแคสต์">พอดแคสต์</a></div>
								<h3><a href="podcast.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...">ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...</a></h3>

								<div class="tools">
									<span class="date">22/01/2563</span>  | 
									<span class="view"><i class="fas fa-eye"></i> 30</span>
								</div>
							</div>
						</div>
					</article>
					<? } ?>
				</div>


			</section>
		</div>
		

  </div>
</div>
<!-- footer -->
<?php include("incs/footer.html") ?>
<?php /*?><?php include("incs/lightbox.html") ?><?php */?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js.html") ?>
<!-- /js -->

</body>
</html>
