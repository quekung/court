<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top.html") ?>
<script type="text/javascript">
//<![CDATA[
document.write('<link href="cs/animate.css" rel="stylesheet" type="text/css">');
//]]>
</script>

<!-- /Top Head -->

<body>
<script>
  //<![CDATA[
  $(document).ready(function(){
	  $('#navigation>ul>li:nth-child(2)>a').addClass('selected');
  });
  //]]>
</script>
<!-- Headbar -->
<?php include("incs/header.html") ?>
<!-- /Headbar -->
<div class="page-category">


   <div id="toc" class=" pt20-sm pt10-xs">
		<div class="sec-vdo container">
			<div class="thm-news player">
				<article>
						<div class="in">
							<figure>
								<a href="detail.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."><img src="di/banner/cover-vdo.png" alt="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."><i class="ic-play"></i></a>
							</figure>
						</div>
					</article>
			</div>
			<article class="reader vdo row _chd-cl-xs-12-sm-08">
				<header class="hgroup _self-cl-xs-12-sm-04 pt0-xs">
					<p class="tag"><a href="category.php">วิดีโอ</a></p>
					<h1>[ศาลรัฐธรรมนูญ] ความสำคัญ หน้าที่ และอำนาจของศาลรัฐธรรมนูญ</h1>
					<span class="date">15/01/2563</span>
					<div class="tool-bar start-xs">
						<div class="share">
							<span>แชร์ </span>
							<div class="list">
							<a href="#" title="facebook"><i class="ic-sh-fb"></i></a>
							<a href="#" title="facebook"><i class="ic-sh-line"></i></a>
							<a href="#" title="facebook"><i class="ic-sh-tw"></i></a>
							</div>
						</div>
						<div class="view"><i class="ic-view"></i> 678</div>
					</div>
				</header>
				
				<div class="read-body editor">
 
					<p>หลังจากนั้นเราเริ่มสำรวจของสะสมของหน่วยงานอื่นๆ อย่างหอสมุดรัฐสภาสหรัฐอเมริกา (Library of Congress) องค์การบริหารจดหมายเหตุและบันทึกแห่งชาติ (National Archives and Records Administration) เมื่อรวมกับสถาบันสมิธโซเนียน (Smithsonian Institution) ก็เท่ากับเป็น 3 หน่วยงานใหญ่ของโลกที่ทำงานด้านนี้ ทางสถานทูตสหรัฐอเมริกาประจำประเทศไทยก็ช่วยเหลือเป็นอย่างดีเพื่อผลักดันให้งานนี้เกิดขึ้น</p>
					<div class="vdo-info">
						<p><a href="#">สื่อประชาสัมพันธ์ศาลรัฐธรรมนูญ<a href="#"></p>
						<p>[<a href="#">2D animation</a> / <a href="#">Motion Graphics</a>]</p>
						<p>เรื่อง “ความสำคัญ หน้าที่ และอำนาจของศาลรัฐธรรมนูญ”</p>
						<p>ความยาว 10.57 นาที</p>
						<p>ผลิตโดย สำนักงานศาลรัฐธรรมนูญ</p>
					</div>
				</div>
			</article>

			<section class="sec-related wow fadeIn" data-wow-delay="0.5s">
				<div class="head-title border0 start-xs">
					<h2 class="h-line"><a href="#all">เรื่องอื่น ๆ ที่เกี่ยวข้อง</a></h2>
				</div>

				<div class="thm-news-list row _chd-cl-xs-12-sm-03">
					<? for($i=1;$i<=1;$i++){ ?>
					<article>
						<div class="in">
							<figure>
								<a href="detail.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."><img src="di/banner/thm-news-01.png" alt="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."><i class="ic-play"></i></a>
							</figure>
							<div class="detail">
								<div class="cat"><a href="#" title="วิดีโอ">วิดีโอ</a></div>
								<h3><a href="detail-vdo.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...">ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...</a></h3>

								<div class="tools">
									<span class="date">22/01/2563</span>  | 
									<span class="view"><i class="fas fa-eye"></i> 30</span>
								</div>
							</div>
						</div>
					</article>

					<article>
						<div class="in">
							<figure>
								<a href="detail-vdo.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."><img src="di/banner/thm-news-02.png" alt="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."><i class="ic-play"></i></a>
							</figure>
							<div class="detail">
								<div class="cat"><a href="#" title="วิดีโอ">วิดีโอ</a></div>
								<h3><a href="detail.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...">ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...</a></h3>

								<div class="tools">
									<span class="date">22/01/2563</span>  | 
									<span class="view"><i class="fas fa-eye"></i> 30</span>
								</div>
							</div>
						</div>
					</article>

					<article>
						<div class="in">
							<figure>
								<a href="detail-vdo.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."><img src="di/banner/thm-news-03.png" alt="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."><i class="ic-play"></i></a>
							</figure>
							<div class="detail">
								<div class="cat"><a href="#" title="วิดีโอ">วิดีโอ</a></div>
								<h3><a href="detail.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...">ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...</a></h3>

								<div class="tools">
									<span class="date">22/01/2563</span>  | 
									<span class="view"><i class="fas fa-eye"></i> 30</span>
								</div>
							</div>
						</div>
					</article>

					<article>
						<div class="in">
							<figure>
								<a href="detail-vdo.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."><img src="di/banner/cover-news.png" alt="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."><i class="ic-play"></i></a>
							</figure>
							<div class="detail">
								<div class="cat"><a href="#" title="วิดีโอ">วิดีโอ</a></div>
								<h3><a href="detail.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...">ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...</a></h3>

								<div class="tools">
									<span class="date">22/01/2563</span>  | 
									<span class="view"><i class="fas fa-eye"></i> 30</span>
								</div>
							</div>
						</div>
					</article>
					<? } ?>
				</div>


			</section>
		</div>
		

  </div>
</div>
<!-- footer -->
<?php include("incs/footer.html") ?>
<?php /*?><?php include("incs/lightbox.html") ?><?php */?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js.html") ?>
<!-- /js -->

</body>
</html>
