<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top.html") ?>
<script type="text/javascript">
//<![CDATA[
document.write('<link href="cs/animate.css" rel="stylesheet" type="text/css">');
//]]>
</script>

<!-- /Top Head -->

<body>
<script>
  //<![CDATA[
  $(document).ready(function(){
	  $('#navigation>ul>li:nth-child(1)>a').addClass('selected');
  });
  //]]>
</script>
<!-- Headbar -->
<?php include("incs/header.html") ?>
<!-- /Headbar -->
<div class="page-home">

<!-- slider -->
<div class="main-slider slick-slider slick-dotted wow fadeIn" data-wow-delay="0.5s" role="toolbar">
    <div class="slider center variable-width slide-banner">
        
            	<article>
					<a href="detail.php?nid=11611" onclick="javascript:ck1(136)"><img src="di/banner/bigslide1.png" class="img-responsive w-100" alt="Second slide"></a>
				</article>
				
            	<article>
					<a href="detail.php?nid=11611" onclick="javascript:ck1(136)"><img src="http://www.constitutionalcourt.or.th/occ_web/images/constitutionalcourt/Banner/2020/banner 22th2  (2).jpg" class="img-responsive w-100" alt="First slide"></a>
				</article>
				
				<article>
					<a href="detail.php?nid=11611" onclick="javascript:ck1(136)"><img src="http://www.constitutionalcourt.or.th/occ_web/images/constitutionalcourt/Banner/prakad2.png" class="img-responsive w-100" alt="Second slide"></a>
				</article>

				<article>
					<a href="detail.php?nid=11611" onclick="javascript:ck1(136)"><img src="http://www.constitutionalcourt.or.th/occ_web/images/constitutionalcourt/Banner/bannerradio.jpg" class="img-responsive w-100" alt="First slide"></a>
				</article>

            	<article>
					<a href="detail.php?nid=11611" onclick="javascript:ck1(136)"><img src="http://www.constitutionalcourt.or.th/occ_web/images/constitutionalcourt/efilling.png" class="img-responsive w-100" alt="First slide"></a>
				</article>

				<article>
					<a href="detail.php?nid=11611" onclick="javascript:ck1(136)"><img src="http://www.constitutionalcourt.or.th/occ_web/images/constitutionalcourt/Banner/2020/banner03.png" class="img-responsive w-100" alt="First slide"></a>
				</article>
    </div>
</div>
		<!-- /slider -->
   <div id="toc">
		<div class="container">

		
		<section class="sec-01 wow fadeIn" data-wow-delay="0.5s">
			<div class="head-title start-xs">
				<h2 class="h-line"><a href="category.php" class="arrow">บทความล่าสุด</a></h2>
			</div>
			<div class="thm-news row _chd-cl-xs-12-sm-04">
				
				<article>
					<div class="in">
						<figure>
							<a href="detail.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."><img src="di/banner/thm-news-01.png" alt="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."></a>
						</figure>
						<div class="detail">
							<div class="cat"><a href="#" title="ข่าวประชาสัมพันธ์">ข่าวประชาสัมพันธ์</a></div>
							<h3><a href="detail.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...">ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...</a></h3>
							
							<div class="tools">
								<span class="date">22/01/2563</span>  | 
								<span class="view"><i class="fas fa-eye"></i> 30</span>
							</div>
						</div>
					</div>
				</article>
				
				<article>
					<div class="in">
						<figure>
							<a href="detail.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."><img src="di/banner/thm-news-02.png" alt="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."></a>
						</figure>
						<div class="detail">
							<div class="cat"><a href="#" title="ข่าวประชาสัมพันธ์">ข่าวประชาสัมพันธ์</a></div>
							<h3><a href="detail.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...">ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...</a></h3>
							
							<div class="tools">
								<span class="date">22/01/2563</span>  | 
								<span class="view"><i class="fas fa-eye"></i> 30</span>
							</div>
						</div>
					</div>
				</article>
				
				<article>
					<div class="in">
						<figure>
							<a href="detail.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."><img src="di/banner/thm-news-03.png" alt="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."></a>
						</figure>
						<div class="detail">
							<div class="cat"><a href="#" title="ข่าวประชาสัมพันธ์">ข่าวประชาสัมพันธ์</a></div>
							<h3><a href="detail.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...">ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...</a></h3>
							
							<div class="tools">
								<span class="date">22/01/2563</span>  | 
								<span class="view"><i class="fas fa-eye"></i> 30</span>
							</div>
						</div>
					</div>
				</article>
				
			</div>
			
			<div class="thm-news-list row _chd-cl-xs-12-sm-03">
				
				<article>
					<div class="in">
						<figure>
							<a href="detail.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."><img src="di/banner/thm4-01.png" alt="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."></a>
						</figure>
						<div class="detail">
							<div class="cat"><a href="#" title="ข่าวประชาสัมพันธ์">ข่าวประชาสัมพันธ์</a></div>
							<h3><a href="detail.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...">ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...</a></h3>
							
							<div class="tools">
								<span class="date">22/01/2563</span>  | 
								<span class="view"><i class="fas fa-eye"></i> 30</span>
							</div>
						</div>
					</div>
				</article>
				
				<article>
					<div class="in">
						<figure>
							<a href="detail.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."><img src="di/banner/thm4-02.png" alt="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."></a>
						</figure>
						<div class="detail">
							<div class="cat"><a href="#" title="ข่าวประชาสัมพันธ์">ข่าวประชาสัมพันธ์</a></div>
							<h3><a href="detail.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...">ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...</a></h3>
							
							<div class="tools">
								<span class="date">22/01/2563</span>  | 
								<span class="view"><i class="fas fa-eye"></i> 30</span>
							</div>
						</div>
					</div>
				</article>
				
				<article>
					<div class="in">
						<figure>
							<a href="detail.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."><img src="di/banner/thm4-03.png" alt="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."></a>
						</figure>
						<div class="detail">
							<div class="cat"><a href="#" title="ข่าวประชาสัมพันธ์">ข่าวประชาสัมพันธ์</a></div>
							<h3><a href="detail.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...">ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...</a></h3>
							
							<div class="tools">
								<span class="date">22/01/2563</span>  | 
								<span class="view"><i class="fas fa-eye"></i> 30</span>
							</div>
						</div>
					</div>
				</article>
				
				<article>
					<div class="in">
						<figure>
							<a href="detail.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."><img src="di/banner/thm4-04.png" alt="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."></a>
						</figure>
						<div class="detail">
							<div class="cat"><a href="#" title="ข่าวประชาสัมพันธ์">ข่าวประชาสัมพันธ์</a></div>
							<h3><a href="detail.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...">ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...</a></h3>
							
							<div class="tools">
								<span class="date">22/01/2563</span>  | 
								<span class="view"><i class="fas fa-eye"></i> 30</span>
							</div>
						</div>
					</div>
				</article>
				
			</div>
		</section>
		
		<section class="sec-02 wow fadeIn" data-wow-delay="0.5s">
			<div class="head-title between-xs">
				<h2 class="h-line"><a href="category.php" class="arrow">สาระศาลรัฐธรรมนูญ</a></h2>
			</div>
			<div class="row reverse _chd-cl-xs-12-sm-05-md-04">
				<div class="thm-rec-big _self-cl-xs-12-sm-07-md-08">

					<article class="_self-mb0">
						<a class="in">
							<figure>
								<span>
									<img src="di/banner/cover-article.png" alt="ประวัติและที่มาของรัฐธรรมนูญแห่งประเทศไทย">
								</span>
							</figure>
							<div class="detail">
								<h3>ประวัติและที่มาของรัฐธรรมนูญแห่งประเทศไทย</h3>
								<p>คณะรณรงค์เพื่อรัฐธรรมนูญฉบับประชาชน (ครช.) จึงเห็นว่าหนทางที่จะล้ม “ระบอบ คสช.” และพาประเทศไทยกลับสู่ “ประชาธิปไตย”</p>
								<span class="more">เพิ่มเติม <i class="ic-more"></i></span>
							</div>
						</a>
					</article>

				</div>
				<div class="thm-rec h-100">

					<article class="h-100 _self-mb0">
						<a class="in">
							<figure>
								<span>
									<img src="di/banner/thm-rec.png" alt="ร่างรัฐธรรมนูญใหม่ ให้ประชาชนเป็นผู้เขียน">
								</span>
							</figure>
							<div class="detail">
								<h3>ร่างรัฐธรรมนูญใหม่ ให้ประชาชนเป็นผู้เขียน</h3>
								<p>แม้ปัจจุบันดูเหมือนประเทศไทยจะคืนกลับสู่ความเป็นประชาธิปไตย คือ มีสภาผู้แทนราษฎรจากการเลือกตั้งและมีพรรคการเมือง...</p>
								<span class="more">เพิ่มเติม <i class="ic-more"></i></span>
							</div>
						</a>
					</article>

				</div>
				
			</div>
		</section>
		
		<div class="full-width bg-dark">
		<section class="sec-03 container wow fadeIn" data-wow-delay="0.5s">
			<div class="head-title start-xs">
				<h2 class="h-line"><a href="category.php" class="arrow">คลิปวิดีโอ</a></h2>
			</div>
			<div class="row _chd-cl-xs-12-sm-06-pd0">
				<div class="thm-news vdo-big _chd-cl-xs-12">
					<article class="big">
						<div class="in">
							<figure>
								<a href="detail-vdo.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."><img src="di/banner/cover-vdo.png" alt="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล..."><i class="ic-play"></i></a>
							</figure>
							<div class="detail">
								<div class="cat"><a href="#" title="วิดีโอ">วิดีโอ</a></div>
								<h3><a href="detail.php" title="ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...">ประธานศาลรัฐธรรมนูญ นำคณะตุลาการศาลรัฐธรรมนูญ และผู้บริหารสำนักงานศาล...</a></h3>

								<div class="tools">
									<span class="date">22/01/2563</span>  | 
									<span class="view"><i class="fas fa-eye"></i> 30</span>
								</div>
							</div>
						</div>
					</article>
				</div>
				
				<div class="thm-news-list vdo _chd-cl-xs-12-sm-06">
				
				

				<article>
					<div class="in">
						<figure>
							<a href="detail.php" title="[ศาลรัฐธรรมนูญ] ความสำคัญ หน้าที่ และอำนาจของศาลรัฐธรรมนูญ"><img src="di/banner/thm-vdo1.png" alt="[ศาลรัฐธรรมนูญ] ความสำคัญ หน้าที่ และอำนาจของศาลรัฐธรรมนูญ"><i class="ic-play"></i></a>
						</figure>
						<div class="detail">
							<div class="cat"><a href="#" title="วิดีโอ">วิดีโอ</a></div>
							<h3><a href="detail-vdo.php" title="[ศาลรัฐธรรมนูญ] ความสำคัญ หน้าที่ และอำนาจของศาลรัฐธรรมนูญ.">[ศาลรัฐธรรมนูญ] ความสำคัญ หน้าที่ และอำนาจของศาลรัฐธรรมนูญ</a></h3>
							
							<div class="tools">
								<span class="date">22/01/2563</span>  | 
								<span class="view"><i class="fas fa-eye"></i> 350</span>
							</div>
						</div>
					</div>
				</article>
				
				<article>
					<div class="in">
						<figure>
							<a href="detail.php" title="[ศาลรัฐธรรมนูญ] ความสำคัญ หน้าที่ และอำนาจของศาลรัฐธรรมนูญ"><img src="di/banner/thm-vdo2.png" alt="[ศาลรัฐธรรมนูญ] ความสำคัญ หน้าที่ และอำนาจของศาลรัฐธรรมนูญ"><i class="ic-play"></i></a>
						</figure>
						<div class="detail">
							<div class="cat"><a href="#" title="วิดีโอ">วิดีโอ</a></div>
							<h3><a href="detail-vdo.php" title="[ศาลรัฐธรรมนูญ] ความสำคัญ หน้าที่ และอำนาจของศาลรัฐธรรมนูญ.">[ศาลรัฐธรรมนูญ] ความสำคัญ หน้าที่ และอำนาจของศาลรัฐธรรมนูญ</a></h3>
							
							<div class="tools">
								<span class="date">22/01/2563</span>  | 
								<span class="view"><i class="fas fa-eye"></i> 350</span>
							</div>
						</div>
					</div>
				</article>
				
				<article>
					<div class="in">
						<figure>
							<a href="detail.php" title="[ศาลรัฐธรรมนูญ] ความสำคัญ หน้าที่ และอำนาจของศาลรัฐธรรมนูญ"><img src="di/banner/thm-vdo3.png" alt="[ศาลรัฐธรรมนูญ] ความสำคัญ หน้าที่ และอำนาจของศาลรัฐธรรมนูญ"><i class="ic-play"></i></a>
						</figure>
						<div class="detail">
							<div class="cat"><a href="#" title="วิดีโอ">วิดีโอ</a></div>
							<h3><a href="detail-vdo.php" title="[ศาลรัฐธรรมนูญ] ความสำคัญ หน้าที่ และอำนาจของศาลรัฐธรรมนูญ.">[ศาลรัฐธรรมนูญ] ความสำคัญ หน้าที่ และอำนาจของศาลรัฐธรรมนูญ</a></h3>
							
							<div class="tools">
								<span class="date">22/01/2563</span>  | 
								<span class="view"><i class="fas fa-eye"></i> 350</span>
							</div>
						</div>
					</div>
				</article>
				
				<article>
					<div class="in">
						<figure>
							<a href="detail-vdo.php" title="[ศาลรัฐธรรมนูญ] ความสำคัญ หน้าที่ และอำนาจของศาลรัฐธรรมนูญ"><img src="di/banner/thm-vdo4.png" alt="[ศาลรัฐธรรมนูญ] ความสำคัญ หน้าที่ และอำนาจของศาลรัฐธรรมนูญ"><i class="ic-play"></i></a>
						</figure>
						<div class="detail">
							<div class="cat"><a href="#" title="วิดีโอ">วิดีโอ</a></div>
							<h3><a href="detail.php" title="[ศาลรัฐธรรมนูญ] ความสำคัญ หน้าที่ และอำนาจของศาลรัฐธรรมนูญ.">[ศาลรัฐธรรมนูญ] ความสำคัญ หน้าที่ และอำนาจของศาลรัฐธรรมนูญ</a></h3>
							
							<div class="tools">
								<span class="date">22/01/2563</span>  | 
								<span class="view"><i class="fas fa-eye"></i> 350</span>
							</div>
						</div>
					</div>
				</article>
				
			</div>
				
			</div>
			

		</section>
		</div>
		
		<section class="sec-04 pt0">
			<div class="head-title center-xs start-md wow fadeIn" data-wow-delay="0.5s">
				<h2 class="h-line"><a href="category.php" class="arrow">พอดแคสต์</a></h2>
			</div>
			<div class="full-width">
			<!-- slider -->
			<div class="pod-slider slick-slider slick-dotted"  role="toolbar">
				<div class="slider rec-pod thm-pod variable-width">
				<? for($i=1;$i<=3;$i++){ ?>
				<article>
					<div class="in">
						<figure>
							<a href="podcast.php" title="[ศาลรัฐธรรมนูญ] ความสำคัญ หน้าที่ และอำนาจของศาลรัฐธรรมนูญ"><img src="di/banner/thm-podcast.png" alt="[ศาลรัฐธรรมนูญ] ความสำคัญ หน้าที่ และอำนาจของศาลรัฐธรรมนูญ"></a>
						</figure>
						<div class="detail">
							<div class="cat"><a href="#" title="พอดแคสต์">พอดแคสต์</a></div>
							<h3><a href="detail.php" title="[ศาลรัฐธรรมนูญ] ความสำคัญ หน้าที่ และอำนาจของศาลรัฐธรรมนูญ">[ศาลรัฐธรรมนูญ] ความสำคัญ หน้าที่ และอำนาจของศาลรัฐธรรมนูญ</a></h3>
							
							<div class="tools">
								<span class="date">22/01/2563</span>  | 
								<span class="view"><i class="fas fa-eye"></i> 350</span>
							</div>
						</div>
					</div>
				</article>
				
				<article>
					<div class="in">
						<figure>
							<a href="podcast.php" title="[ศาลรัฐธรรมนูญ] ความสำคัญ หน้าที่ และอำนาจของศาลรัฐธรรมนูญ"><img src="di/banner/thm-podcast2.png" alt="[ศาลรัฐธรรมนูญ] ความสำคัญ หน้าที่ และอำนาจของศาลรัฐธรรมนูญ"></a>
						</figure>
						<div class="detail">
							<div class="cat"><a href="#" title="พอดแคสต์">พอดแคสต์</a></div>
							<h3><a href="detail.php" title="[ศาลรัฐธรรมนูญ] ความสำคัญ หน้าที่ และอำนาจของศาลรัฐธรรมนูญ">[ศาลรัฐธรรมนูญ] ความสำคัญ หน้าที่ และอำนาจของศาลรัฐธรรมนูญ</a></h3>
							
							<div class="tools">
								<span class="date">22/01/2563</span>  | 
								<span class="view"><i class="fas fa-eye"></i> 350</span>
							</div>
						</div>
					</div>
				</article>
				<? } ?>
				</div>
			</div>
			<!-- /slider -->
			</div>
		</section>
		

		</div>
  </div>
</div>
<!-- footer -->
<?php include("incs/footer.html") ?>
<?php include("incs/lightbox.html") ?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js.html") ?>

<script>
$('.slide-banner').slick({
  dots: true,
  centerMode: false,
  //centerPadding: '60px',
  autoplay: true,
  arrows: true,
  infinite: true,
  autoplaySpeed: 5000,
  slidesToShow: 1,
  adaptiveHeight: true,
  //variableWidth: false,
});

$('.rec-pod').slick({
  dots: true,
  infinite: true,
  arrows: true,
  centerMode: true,
  speed: 300,
  autoplaySpeed: 5000,
  //centerPadding: '80px',
  slidesToShow: 5,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1366,
      settings: {
	  	//centerPadding: '60px',
        slidesToShow: 4,
        slidesToScroll: 1
      }
    },
	{
      breakpoint: 800,
      settings: {
	  	//centerPadding: '60px',
        slidesToShow: 3,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
	  	//centerPadding: '40px',
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});
</script>
<!-- /js -->

</body>
</html>
